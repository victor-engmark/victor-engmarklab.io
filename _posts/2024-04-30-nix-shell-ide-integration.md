---
title: Nix shell IDE integration
categories:
  - Nix
  - Linux
  - software
---

Most projects involve some CLI tools which can be useful to integrate into the
IDE. For example, when using an interpreted language like Python, telling the
IDE where to find the `python` executable means it should be able to find and
auto-complete [language-specific
dependencies]({% post_url 2024-04-30-nix-language-specific-dependencies %}), detect
any syntax errors based on the exact interpreter version, and run the tests without
any extra setup. There's just one snag: some IDEs require you to specify an _absolute_
path to the interpreter, and Nix store paths are hashed:

```console
$ nix-shell --pure --run 'type -a python'
python is /nix/store/y027d3bvlaizbri04c1bzh28hqd6lj01-python3-3.11.7/bin/python
```

Now, we _could_ absolutely point the IDE to this path, but that's brittle: if
anything changes in the Python derivation, this path will change, and the IDE
will no longer be pointing at the right path. We might not even notice until the
path is garbage collected from the Nix store.

Here's the thing, though: _Bash_ knows about `python` because `$PATH` contains
the Nix store path
(`/nix/store/y027d3bvlaizbri04c1bzh28hqd6lj01-python3-3.11.7/bin` in the example
above), and we can use that knowledge to work around IDE deficiencies. If we use
the shell to create (or overwrite) a _symbolic link_ to the `python` executable
every time we activate the Nix shell, that link will always point to the
relevant file. We can do this in the `shellHook`:

```nix
{% include shell-with-interpreter-symlink.nix -%}
```

Now, any time we re-enter this shell, it updates the `python`
symlink[^symlink-directory], and we have a fixed absolute path to the "current"
Python interpreter! Just point to that file in your IDE, and it should detect
the rest. You probably also want to add `/python` to your `.gitignore` to make
sure it doesn't get added to version control.

For reference,
[an example repository which uses this technique](https://github.com/linz/emergency-management-tools/blob/766f835d5d3b85b3202864d7719696659694e1f2/shell.nix#L53).

[^symlink-directory]:
    The symlink ends up in the directory where you run `nix-shell`. Since this
    is normally the root of the project, that's what I assume in this article.
