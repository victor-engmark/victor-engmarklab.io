---
title: On brevity
categories:
  - limerick
  - silliness
---

There was a young joker, a git,<br/>Who thought himself quite the wit,<br/>But
he wasn't funny,<br/>His jokes were runny,<br/>Like “Conciseness, rather than
prolixity, is the very being, the very body, the very soul, of wit!”
