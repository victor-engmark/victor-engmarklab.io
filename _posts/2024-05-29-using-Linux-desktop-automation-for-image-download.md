---
title: Using Linux desktop automation for image download
categories:
  - automation
  - Linux
  - Firefox
---

Intelerad has developed something called "nuage Patient Portal", which is used
by many clinics to let patients download their scans. It's an excellent service,
especially if you want to have backups of all of your own health records. But it
(or at least the version used by my clinic) does have a flaw: it's cumbersome to
download all the images.

It _is_ possible to download an image set as a video, but the resulting file
seems to be broken. Popular video software like MPV and VLC were unable to play
the files smoothly, and using FFmpeg to extract frames from the video resulted
in an incomplete set, so I'm pretty sure the video files are not standards
compliant. It is also possible to download each image of a set individually, but
this requires two mouse clicks and then scrolling or pressing the down button to
get to the next image. Doing this for more than 1,000 images would be a massive
chore, not to mention error prone. So I looked around for a way to easily
automate this (ideally) one-off process. I tried a few solutions, but none of
them seemed to work well, so I won't mention them here.

After a few hours I learned about
[`ydotool`](https://github.com/ReimuNotMoe/ydotool), a generic desktop
automation tool for Linux. It did the trick, by providing simple commands like
`ydotool click 0xC0` to left click and release,
`ydotool mousemove --xpos 0 --ypos -50` to move the mouse pointer down, and
`ydotool key 108:1 108:0` to press and immediately release the down arrow.

## Prerequisites

- Bash
- ydotool

## Code

```bash
{% include nuage-download.bash -%}
```

## Use

1. Put the code above into a file such as `download.bash`.
1. Make the file executable.
1. Run the file and follow the instructions printed on screen.
