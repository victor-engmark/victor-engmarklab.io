my_command() {
    echo 'stdout content'
    echo 'stderr content' >&2
}

# Bad commit because "stdout" occurs on a different file descriptor
{
    my_command 3>&2 2>&1 1>&3- |
        tee -p /dev/fd/4 |
        grep --quiet stdout
} 3>&2 2>&1 1>&3- 4>&1

# Good commit because it finds "stderr"
{
    my_command 3>&2 2>&1 1>&3- |
        tee -p /dev/fd/4 |
        grep --quiet stderr
} 3>&2 2>&1 1>&3- 4>&1

# Bad commit because "other" does not appear anywhere
{
    my_command 3>&2 2>&1 1>&3- |
        tee -p /dev/fd/4 |
        grep --quiet other
} 3>&2 2>&1 1>&3- 4>&1

# Prints "stderr content"
{
    {
        my_command 3>&2 2>&1 1>&3- |
            tee -p /dev/fd/4 |
            grep --quiet stdout
    } 3>&2 2>&1 1>&3- 4>&1
} > /dev/null

# Prints "stdout content"
{
    {
        my_command 3>&2 2>&1 1>&3- |
            tee -p /dev/fd/4 |
            grep --quiet stdout
    } 3>&2 2>&1 1>&3- 4>&1
} 2> /dev/null
