(function () {
  if (
    (channelId =
      window?.ytInitialPlayerResponse?.videoDetails?.channelId ??
      document
        .querySelector(
          "link[rel='canonical'][href^='https://www.youtube.com/channel/']",
        )
        ?.getAttribute("href")
        ?.substring(32))
  ) {
    console.debug("Going to feed URL");
    location.href =
      "https://www.youtube.com/feeds/videos.xml?channel_id=" + channelId;
  } else {
    console.warn("Could not find a channel ID feed at " + location.href);
  }
})();
