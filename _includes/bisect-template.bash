cat > bisect.bash << 'EOF'
#!/usr/bin/env bash

set -o errexit # Exit on the first non-zero exit code
set -o noclobber # Don't allow redirecting to existing files, for safety
set -o nounset # Treat dereferencing non-existing variables as an error
set -o pipefail # Fail fast when using pipes
shopt -s failglob # Fail when a glob does not match any files
shopt -s inherit_errexit # Apply errexit setting to command substitutions

# Setup & build
# For example, (re-)install prerequisites, reset DB, etc
# Exit code 125 means the code can't be tested, and Git skips the current commit
COMMAND || exit 125
[…]

# Test
COMMAND
[…]
EOF
chmod u+x bisect.bash
