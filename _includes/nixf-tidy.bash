#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

for file; do
    result="$(nixf-tidy --pretty-print --variable-lookup < "${file}")"
    if [[ "${result}" != '[]' ]]; then
        printf '%s: %s\n' "${file}" "${result}" >&2
        exit_code=3
    fi
done

exit "${exit_code-0}"
