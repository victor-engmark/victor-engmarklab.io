---
layout: page
title: About
permalink: /about/
---

## Me

I’m a professional and hobby software developer. You can contact me via
[email](mailto:feedback@paperless.blog){:rel="me"}. Optional PGP key:
[F1C5 F630 13E5 DE2F B574 D42A CE5E B383 44D8 5DB6](https://keys.openpgp.org/vks/v1/by-fingerprint/F1C5F63013E5DE2FB574D42ACE5EB38344D85DB6)

## Technology

This site is [built](https://gitlab.com/engmark/engmark.gitlab.io) using:

- [check-jsonschema](https://github.com/python-jsonschema/check-jsonschema)
- [Git](https://git-scm.com/)
- [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- [gitlint](https://jorisroovers.com/gitlint/)
- [GoatCounter](https://www.goatcounter.com/)
  ([sponsored](https://github.com/sponsors/arp242/))
- [html-proofer](https://github.com/gjtorikian/html-proofer)
- [ImageMagick](https://imagemagick.org/)
- [Jekyll](https://jekyllrb.com/)
- [Mocha](https://mochajs.org/)
- [Nix](https://nixos.org/)
  ([sponsored](https://opencollective.com/victor-engmark))
- [nixfmt](https://nixfmt.serokell.io/)
- [Node.js](https://nodejs.org/)
- [pre-commit](https://pre-commit.com/)
- [Prettier](https://prettier.io/)
- [ShellCheck](https://www.shellcheck.net/)
- [shfmt](https://github.com/mvdan/sh)
- [Vale](https://vale.sh/)
- [yq](https://mikefarah.gitbook.io/yq)

## Thanks

Thank you to GitLab for hosting all of this for free!

A huge thank you to all the people building the free and open source software
used for this project!

## License

All content on this site is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/){:rel="license"}.
The source code is licensed under
[GNU Affero General Public License version 3](https://www.gnu.org/licenses/agpl-3.0.html){:rel="license"}
or newer.
